#include "TGADrawer.h"
#include <utility>
#include <vector>

/************************************************************************/
/* ������ ����� ��������� ����� ���� ��������� ������ � ����� ��������� */
/************************************************************************/
void TGADrawer::drawLine(int x0, int y0, int x1, int y1, TGAImage & image, TGAColor color)
{
	bool steep = std::abs(y1 - y0) > std::abs(x1 - x0);

	if (steep) {
		std::swap(x0, y0);
		std::swap(x1, y1);
	}

	if (x0 > x1) {
		std::swap(x0, x1);
		std::swap(y0, y1);
	}

	int dx = x1 - x0;
	int dy = std::abs(y1 - y0);
	int error = dx / 2;
	int ystep = (y0 < y1) ? 1 : -1;
	int y = y0;

	for (int x = x0; x <= x1; x++) {
		image.set(steep ? y : x, steep ? x : y, color);
		error -= dy;
		if (error < 0)
		{
			y += ystep;
			error += dx;
		}
	}
}

/************************************************************************/
/* ������ ����� ��������� ���������� ���� �������� ������ � ����� ����� */
/************************************************************************/
void TGADrawer::drawLine(Vec2i t0, Vec2i t1, TGAImage & image, TGAColor color)
{
	TGADrawer::drawLine(t0.x, t0.y, t1.x, t1.y, image, color);
}

void TGADrawer::drawWarefront(int width, int height, Model * model, TGAImage & image, TGAColor color)
{
	for (int i = 0; i < model->nfaces(); i++) {
		std::vector<int> face = model->face(i);
		for (int j = 0; j < 3; j++) {
			Vec3f v0 = model->vert(face[j]);
			Vec3f v1 = model->vert(face[(j + 1) % 3]);
			int x0 = (v0.x + 1.)*width / 2.;
			int y0 = (v0.y + 1.)*height / 2.;
			int x1 = (v1.x + 1.)*width / 2.;
			int y1 = (v1.y + 1.)*height / 2.;
			TGADrawer::drawLine(x0, y0, x1, y1, image, color);
		}
	}
}

void TGADrawer::drawTriangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage & image, TGAColor color)
{
	TGADrawer::drawLine(t0, t1, image, color);
	TGADrawer::drawLine(t1, t2, image, color);
	TGADrawer::drawLine(t2, t0, image, color);
}

void TGADrawer::drawFillTriangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage &image, TGAColor color)
{
	if (t0.y == t1.y && t0.y == t2.y) {
		return;
	}

	if (t0.y > t1.y) std::swap(t0, t1);
	if (t0.y > t2.y) std::swap(t0, t2);
	if (t1.y > t2.y) std::swap(t1, t2);

	int total_height = t2.y - t0.y;

	for (int i = 0; i<total_height; i++) {
		bool second_half = i>t1.y - t0.y || t1.y == t0.y;
		int segment_height = second_half ? t2.y - t1.y : t1.y - t0.y;

		float alpha = (float)i / total_height;
		float beta = (float)(i - (second_half ? t1.y - t0.y : 0)) / segment_height;

		Vec2i A = t0 + (t2 - t0)*alpha;
		Vec2i B = second_half ? t1 + (t2 - t1)*beta : t0 + (t1 - t0)*beta;

		if (A.x > B.x) std::swap(A, B);

		for (int j = A.x; j <= B.x; j++) {
			image.set(j, t0.y + i, color);
		}
	}
}