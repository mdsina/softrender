#include "tgaimage.h"
#include "TGADrawer.h"
#include "model.h"
#include "RenderUtils.h"

const TGAColor white = TGAColor(255, 255, 200, 255);
const TGAColor red   = TGAColor(255, 0,   0,   255);

int main(int argc, char** argv) {
	TGAImage image(800, 800, TGAImage::RGB);

	Model * model = new Model("obj/african_head.obj");

	for (int i = 0; i < model->nfaces(); i++) {
		std::vector<int> face = model->face(i);
		Vec2i screen_coords[3];
		for (int j = 0; j < 3; j++) {
			Vec3f world_coords = model->vert(face[j]);
			screen_coords[j] = RenderUtils::worldToSceen(world_coords, 800, 800);
		}
		TGADrawer::drawFillTriangle(screen_coords[0], screen_coords[1], screen_coords[2], image, TGAColor(rand() % 255, rand() % 255, rand() % 255, 255));
	}

	//TGADrawer::drawWarefront(400, 400, model, image, white);

// 	Vec2i t0[3] = { Vec2i(10, 70), Vec2i(50, 160), Vec2i(70, 80) };
// 	Vec2i t1[3] = { Vec2i(180, 50), Vec2i(150, 1), Vec2i(70, 180) };
// 	Vec2i t2[3] = { Vec2i(180, 150), Vec2i(120, 160), Vec2i(130, 180) };
// 
// 	TGADrawer::drawFillTriangle(t0[0], t0[1], t0[2], image, red);
// 	TGADrawer::drawFillTriangle(t1[0], t1[1], t1[2], image, white);
// 	TGADrawer::drawFillTriangle(t2[0], t2[1], t2[2], image, red);
	image.flip_vertically();
	image.write_tga_file("output1.tga");

	delete model;
	return 0;
}

