#ifndef __RENDER_UTILS_H__
#define __RENDER_UTILS_H__

#include "geometry.h"

class RenderUtils
{
public:
	RenderUtils() {};
	~RenderUtils() {};

	Vec2i static worldToSceen(Vec2f worldCoords, int width, int height);
	Vec2i static worldToSceen(Vec3f worldCoords, int width, int height);
protected:
private:
};

#endif /* __RENDER_UTILS_H__ */