#ifndef __TGA_DRAWER_H__
#define __TGA_DRAWER_H__

#include "tgaimage.h"
#include "model.h"

class TGADrawer
{
public:
	TGADrawer() {}
	~TGADrawer() {}
	void static drawLine(int x0, int x1, int y0, int y1, TGAImage & image, TGAColor color);
	void static drawLine(Vec2i t0, Vec2i t1, TGAImage & image, TGAColor color);

	void static drawWarefront(int width, int height, Model * model, TGAImage & image, TGAColor color);

	void static drawTriangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage & image, TGAColor color);

	void static drawFillTriangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage &image, TGAColor color);
protected:
private:
};

#endif /* __TGA_DRAWER_H__ */