#include "RenderUtils.h"


Vec2i RenderUtils::worldToSceen(Vec3f worldCoords, int width, int height)
{
	return Vec2i((worldCoords.x + 1.)*width / 2., (worldCoords.y + 1.)*height / 2.);
}

Vec2i RenderUtils::worldToSceen(Vec2f worldCoords, int width, int height)
{
	return Vec2i((worldCoords.x + 1.)*width / 2., (worldCoords.y + 1.)*height / 2.);
}